# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-02 09:13-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: appMenu.js:38 appMenu.js:230
msgid "Pin to ArcMenu"
msgstr ""

#: appMenu.js:61 appMenu.js:186
msgid "Create Desktop Shortcut"
msgstr ""

#: appMenu.js:186
msgid "Delete Desktop Shortcut"
msgstr ""

#: appMenu.js:194 appMenu.js:230
msgid "Unpin from ArcMenu"
msgstr ""

#: appMenu.js:267
msgid "Open Folder Location"
msgstr ""

#: constants.js:65
msgid "Favorites"
msgstr ""

#: constants.js:66 menulayouts/runner.js:92 menulayouts/tweaks/tweaks.js:721
msgid "Frequent Apps"
msgstr ""

#: constants.js:67 menulayouts/az.js:128 menulayouts/az.js:154
#: menulayouts/eleven.js:159 menulayouts/eleven.js:217 menuWidgets.js:1050
msgid "All Apps"
msgstr ""

#: constants.js:68 menulayouts/insider.js:156 menulayouts/plasma.js:117
#: menulayouts/raven.js:55 menulayouts/raven.js:226 menulayouts/raven.js:275
#: menulayouts/tweaks/tweaks.js:719 menulayouts/unity.js:37
#: menulayouts/unity.js:290 menulayouts/unity.js:349 menulayouts/windows.js:381
#: menuWidgets.js:716 prefs.js:3527 prefs.js:3661 utils.js:273
msgid "Pinned Apps"
msgstr ""

#: constants.js:69 menulayouts/raven.js:297 menulayouts/unity.js:367
#: searchProviders/recentFiles.js:26 searchProviders/recentFiles.js:27
msgid "Recent Files"
msgstr ""

#: constants.js:229 menulayouts/mint.js:222 menulayouts/unity.js:213
#: prefs.js:343
msgid "Log Out"
msgstr ""

#: constants.js:230 menulayouts/mint.js:223 menulayouts/unity.js:214
#: prefs.js:342
msgid "Lock"
msgstr ""

#: constants.js:231 prefs.js:345
msgid "Restart"
msgstr ""

#: constants.js:232 menulayouts/mint.js:224 menulayouts/unity.js:215
#: prefs.js:344
msgid "Power Off"
msgstr ""

#: constants.js:233 prefs.js:346
msgid "Suspend"
msgstr ""

#: constants.js:234 prefs.js:347
msgid "Hybrid Sleep"
msgstr ""

#: constants.js:235 prefs.js:348
msgid "Hibernate"
msgstr ""

#: constants.js:357 prefs.js:2877
msgid "ArcMenu"
msgstr ""

#: constants.js:358
msgid "Brisk"
msgstr ""

#: constants.js:359
msgid "Whisker"
msgstr ""

#: constants.js:360
msgid "GNOME Menu"
msgstr ""

#: constants.js:361
msgid "Mint"
msgstr ""

#: constants.js:362
msgid "Budgie"
msgstr ""

#: constants.js:365
msgid "Unity"
msgstr ""

#: constants.js:366
msgid "Plasma"
msgstr ""

#: constants.js:367
msgid "tognee"
msgstr ""

#: constants.js:368
msgid "Insider"
msgstr ""

#: constants.js:369
msgid "Redmond"
msgstr ""

#: constants.js:370
msgid "Windows"
msgstr ""

#: constants.js:371
msgid "11"
msgstr ""

#: constants.js:372
msgid "a.z."
msgstr ""

#: constants.js:375
msgid "Elementary"
msgstr ""

#: constants.js:376
msgid "Chromebook"
msgstr ""

#: constants.js:379
msgid "Runner"
msgstr ""

#: constants.js:380
msgid "GNOME Overview"
msgstr ""

#: constants.js:383
msgid "Raven"
msgstr ""

#: constants.js:387
msgid "Traditional"
msgstr ""

#: constants.js:388
msgid "Modern"
msgstr ""

#: constants.js:389
msgid "Touch"
msgstr ""

#: constants.js:390
msgid "Launcher"
msgstr ""

#: constants.js:391
msgid "Alternative"
msgstr ""

#: menuButton.js:552 menulayouts/arcmenu.js:178 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:149
#: menulayouts/windows.js:119 menuWidgets.js:1505 prefs.js:318 prefs.js:332
#: prefs.js:2672 prefs.js:3805
msgid "ArcMenu Settings"
msgstr ""

#: menuButton.js:556 prefs.js:2049
msgid "General Settings"
msgstr ""

#: menuButton.js:557
msgid "Menu Theming"
msgstr ""

#: menuButton.js:558
msgid "Change Menu Layout"
msgstr ""

#: menuButton.js:559
msgid "Layout Tweaks"
msgstr ""

#: menuButton.js:560
msgid "Customize Menu"
msgstr ""

#: menuButton.js:561 prefs.js:3526 prefs.js:3657
msgid "Button Settings"
msgstr ""

#: menuButton.js:564 prefs.js:2856
msgid "About"
msgstr ""

#: menuButton.js:570
msgid "Dash to Panel Settings"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/baseMenuLayout.js:475
#: menulayouts/baseMenuLayout.js:477 menulayouts/mint.js:217
#: menulayouts/plasma.js:167 menulayouts/raven.js:166
#: menulayouts/redmond.js:143 menulayouts/tognee.js:138
#: menulayouts/unity.js:149 menulayouts/unity.js:208 menulayouts/windows.js:119
msgid "Software"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/az.js:121 menulayouts/eleven.js:150
#: menulayouts/insider.js:58 menulayouts/mint.js:213 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:149 menulayouts/whisker.js:49
#: menulayouts/windows.js:57 menulayouts/windows.js:119
msgid "Settings"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:149
#: menulayouts/windows.js:119
msgid "Tweaks"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/az.js:118 menulayouts/eleven.js:147
#: menulayouts/insider.js:55 menulayouts/mint.js:212 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:149 menulayouts/windows.js:54
#: menulayouts/windows.js:119 menuWidgets.js:1508
msgid "Terminal"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:149
#: menulayouts/windows.js:119 menuWidgets.js:429 prefs.js:331
msgid "Activities Overview"
msgstr ""

#: menulayouts/az.js:114 menulayouts/eleven.js:143 menulayouts/insider.js:52
#: menulayouts/mint.js:221 menulayouts/unity.js:212 menulayouts/windows.js:51
#: menuWidgets.js:1511
msgid "Files"
msgstr ""

#: menulayouts/az.js:129 menulayouts/az.js:148 menulayouts/az.js:205
#: menulayouts/eleven.js:160 menulayouts/eleven.js:207
#: menulayouts/eleven.js:263
msgid "Pinned"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/baseMenuLayout.js:461
#: menulayouts/mint.js:210 menulayouts/tweaks/tweaks.js:461
#: menulayouts/tweaks/tweaks.js:530 menulayouts/unity.js:202
#: placeDisplay.js:336 utils.js:256
msgid "Home"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/mint.js:219
#: menulayouts/unity.js:203
msgid "Documents"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/unity.js:204
msgid "Downloads"
msgstr ""

#: menulayouts/baseMenuLayout.js:417
msgid "Music"
msgstr ""

#: menulayouts/baseMenuLayout.js:417
msgid "Pictures"
msgstr ""

#: menulayouts/baseMenuLayout.js:417
msgid "Videos"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/plasma.js:126
#: menulayouts/unity.js:210 placeDisplay.js:125 placeDisplay.js:148
#: prefs.js:324 prefs.js:340
msgid "Computer"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/baseMenuLayout.js:469
#: menulayouts/plasma.js:289 menulayouts/windows.js:187 prefs.js:325
#: prefs.js:341
msgid "Network"
msgstr ""

#: menulayouts/eleven.js:161 menulayouts/eleven.js:183
#: menulayouts/eleven.js:267 menulayouts/windows.js:323
msgid "Frequent"
msgstr ""

#: menulayouts/plasma.js:122 menuWidgets.js:1008 menuWidgets.js:2401
msgid "Applications"
msgstr ""

#: menulayouts/plasma.js:130 menuWidgets.js:740
msgid "Leave"
msgstr ""

#: menulayouts/plasma.js:257 menulayouts/windows.js:254 prefs.js:3529
#: prefs.js:3684
msgid "Application Shortcuts"
msgstr ""

#: menulayouts/plasma.js:261 menulayouts/windows.js:258
msgid "Places"
msgstr ""

#: menulayouts/plasma.js:273 menulayouts/tweaks/tweaks.js:664
#: menulayouts/tweaks/tweaks.js:766 menulayouts/windows.js:171
msgid "Bookmarks"
msgstr ""

#: menulayouts/plasma.js:281 menulayouts/windows.js:179
msgid "Devices"
msgstr ""

#: menulayouts/plasma.js:316 menuWidgets.js:766
msgid "Session"
msgstr ""

#: menulayouts/plasma.js:321 menuWidgets.js:770
msgid "System"
msgstr ""

#: menulayouts/raven.js:57 menulayouts/raven.js:233
#: menulayouts/tweaks/tweaks.js:462 menulayouts/tweaks/tweaks.js:531
#: menulayouts/tweaks/tweaks.js:698 menulayouts/tweaks/tweaks.js:722
#: menulayouts/unity.js:39 menulayouts/unity.js:295
msgid "All Programs"
msgstr ""

#: menulayouts/raven.js:277 menulayouts/unity.js:351
msgid "Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:34 menuWidgets.js:1078 menuWidgets.js:1111
#: prefs.js:1855
msgid "Back"
msgstr ""

#: menulayouts/tweaks/tweaks.js:119
msgid "Mouse Click"
msgstr ""

#: menulayouts/tweaks/tweaks.js:120
msgid "Mouse Hover"
msgstr ""

#: menulayouts/tweaks/tweaks.js:123
msgid "Category Activation"
msgstr ""

#: menulayouts/tweaks/tweaks.js:146
msgid "Round"
msgstr ""

#: menulayouts/tweaks/tweaks.js:147 prefs.js:2176 prefs.js:2177 prefs.js:2178
msgid "Square"
msgstr ""

#: menulayouts/tweaks/tweaks.js:149
msgid "Avatar Icon Shape"
msgstr ""

#: menulayouts/tweaks/tweaks.js:164 menulayouts/tweaks/tweaks.js:783
msgid "Bottom"
msgstr ""

#: menulayouts/tweaks/tweaks.js:165 menulayouts/tweaks/tweaks.js:356
#: menulayouts/tweaks/tweaks.js:784
msgid "Top"
msgstr ""

#: menulayouts/tweaks/tweaks.js:168
msgid "Searchbar Location"
msgstr ""

#: menulayouts/tweaks/tweaks.js:189
msgid "Flip Layout Horizontally"
msgstr ""

#: menulayouts/tweaks/tweaks.js:205
msgid "Disable User Avatar"
msgstr ""

#: menulayouts/tweaks/tweaks.js:222 menulayouts/tweaks/tweaks.js:259
msgid "Disable Frequent Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:240
msgid "Show Applications Grid"
msgstr ""

#: menulayouts/tweaks/tweaks.js:273
msgid "Disable Pinned Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:294
msgid "Activate on Hover"
msgstr ""

#: menulayouts/tweaks/tweaks.js:310
msgid "Brisk Menu Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:344
msgid "Enable Activities Overview Shortcut"
msgstr ""

#: menulayouts/tweaks/tweaks.js:357
msgid "Centered"
msgstr ""

#: menulayouts/tweaks/tweaks.js:359
msgid "Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:386
msgid "Width"
msgstr ""

#: menulayouts/tweaks/tweaks.js:409 prefs.js:1979
msgid "Height"
msgstr ""

#: menulayouts/tweaks/tweaks.js:432 prefs.js:3226
msgid "Font Size"
msgstr ""

#: menulayouts/tweaks/tweaks.js:433 prefs.js:1116
#, javascript-format
msgid "%d Default Theme Value"
msgstr ""

#: menulayouts/tweaks/tweaks.js:447
msgid "Show Frequent Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:464 menulayouts/tweaks/tweaks.js:533
#: menulayouts/tweaks/tweaks.js:700 menulayouts/tweaks/tweaks.js:724
msgid "Default View"
msgstr ""

#: menulayouts/tweaks/tweaks.js:478
msgid "Unity Layout Buttons"
msgstr ""

#: menulayouts/tweaks/tweaks.js:485
msgid "Button Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:503 menulayouts/tweaks/tweaks.js:593
msgid "Adjust the position of the separator in the button panel"
msgstr ""

#: menulayouts/tweaks/tweaks.js:516 menulayouts/tweaks/tweaks.js:606
msgid "Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:544 prefs.js:633 prefs.js:659
msgid "Left"
msgstr ""

#: menulayouts/tweaks/tweaks.js:545 prefs.js:635 prefs.js:661
msgid "Right"
msgstr ""

#: menulayouts/tweaks/tweaks.js:547
msgid "Position on Monitor"
msgstr ""

#: menulayouts/tweaks/tweaks.js:568
msgid "Mint Layout Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:575
msgid "Shortcut Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:633 menulayouts/tweaks/tweaks.js:741
msgid "Extra Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:650 menulayouts/tweaks/tweaks.js:752
msgid "External Devices"
msgstr ""

#: menulayouts/tweaks/tweaks.js:687
msgid "Nothing Yet!"
msgstr ""

#: menulayouts/tweaks/tweaks.js:697 menulayouts/tweaks/tweaks.js:720
msgid "Categories List"
msgstr ""

#: menulayouts/tweaks/tweaks.js:774
msgid "Extra Categories Quick Links"
msgstr ""

#: menulayouts/tweaks/tweaks.js:775
msgid ""
"Display quick links of extra categories on the home page\n"
"See Customize Menu -> Extra Categories"
msgstr ""

#: menulayouts/tweaks/tweaks.js:786
msgid "Quick Links Location"
msgstr ""

#: menulayouts/tweaks/tweaks.js:819
msgid "Enable Weather Widget"
msgstr ""

#: menulayouts/tweaks/tweaks.js:833
msgid "Enable Clock Widget"
msgstr ""

#: menuWidgets.js:703
msgid "Configure Runner"
msgstr ""

#: menuWidgets.js:728
msgid "Extras"
msgstr ""

#: menuWidgets.js:835
msgid "Categories"
msgstr ""

#: menuWidgets.js:1155
msgid "All Applications"
msgstr ""

#: menuWidgets.js:1807
msgid "New"
msgstr ""

#: menuWidgets.js:2245
msgid "Search…"
msgstr ""

#: placeDisplay.js:49
#, javascript-format
msgid "Failed to launch “%s”"
msgstr ""

#: placeDisplay.js:64
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr ""

#: placeDisplay.js:210
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr ""

#: prefs.js:34 prefs.js:56
msgid "Add More Apps"
msgstr ""

#: prefs.js:45
msgid "Add Default User Directories"
msgstr ""

#: prefs.js:121
msgid "Add Custom Shortcut"
msgstr ""

#: prefs.js:307
msgid "Add to your Pinned Apps"
msgstr ""

#: prefs.js:309
msgid "Change Selected Pinned App"
msgstr ""

#: prefs.js:311
msgid "Select Application Shortcuts"
msgstr ""

#: prefs.js:313
msgid "Select Directory Shortcuts"
msgstr ""

#: prefs.js:326
msgid "Recent"
msgstr ""

#: prefs.js:333
msgid "Run Command..."
msgstr ""

#: prefs.js:334
msgid "Show All Applications"
msgstr ""

#: prefs.js:451
#, javascript-format
msgid "%s has been pinned to ArcMenu"
msgstr ""

#: prefs.js:463
#, javascript-format
msgid "%s has been unpinned from ArcMenu"
msgstr ""

#: prefs.js:496
msgid "Add a Custom Shortcut"
msgstr ""

#: prefs.js:499
msgid "Edit Pinned App"
msgstr ""

#: prefs.js:501
msgid "Edit Shortcut"
msgstr ""

#: prefs.js:511
msgid "Title"
msgstr ""

#: prefs.js:522 prefs.js:1070 prefs.js:1183
msgid "Icon"
msgstr ""

#: prefs.js:532 prefs.js:1490
msgid "Browse..."
msgstr ""

#: prefs.js:538 prefs.js:1495
msgid "Select an Icon"
msgstr ""

#: prefs.js:567
msgid "Command"
msgstr ""

#: prefs.js:570
msgid "Shortcut Path"
msgstr ""

#: prefs.js:580 prefs.js:1017 prefs.js:1841
msgid "Apply"
msgstr ""

#: prefs.js:580
msgid "Add"
msgstr ""

#: prefs.js:605
msgid "General"
msgstr ""

#: prefs.js:612
msgid "Display Options"
msgstr ""

#: prefs.js:625
msgid "Show Activities Button"
msgstr ""

#: prefs.js:634 prefs.js:660
msgid "Center"
msgstr ""

#: prefs.js:637 prefs.js:1139
msgid "Position in Panel"
msgstr ""

#: prefs.js:667
msgid "Menu Alignment"
msgstr ""

#: prefs.js:686
msgid "Display ArcMenu on all Panels"
msgstr ""

#: prefs.js:687
msgid "Dash to Panel extension required"
msgstr ""

#: prefs.js:703
msgid "Always Prefer Top Panel"
msgstr ""

#: prefs.js:704
msgid "Useful with Dash to Panel setting 'Keep original gnome-shell top panel'"
msgstr ""

#: prefs.js:717
msgid "Hotkey Options"
msgstr ""

#: prefs.js:720
msgid "Standalone Runner Menu"
msgstr ""

#: prefs.js:763
msgid "Enable a standalone Runner menu"
msgstr ""

#: prefs.js:778
msgid "Open on Primary Monitor"
msgstr ""

#: prefs.js:785
msgid "None"
msgstr ""

#: prefs.js:786
msgid "Left Super Key"
msgstr ""

#: prefs.js:787
msgid "Custom Hotkey"
msgstr ""

#: prefs.js:790
msgid "Menu Hotkey"
msgstr ""

#: prefs.js:790
msgid "Runner Hotkey"
msgstr ""

#: prefs.js:803
msgid "Modify Hotkey"
msgstr ""

#: prefs.js:808
msgid "Current Hotkey"
msgstr ""

#: prefs.js:892
msgid "Set Custom Hotkey"
msgstr ""

#: prefs.js:917
msgid "Choose Modifiers"
msgstr ""

#: prefs.js:927
msgid "Ctrl"
msgstr ""

#: prefs.js:931
msgid "Super"
msgstr ""

#: prefs.js:935
msgid "Shift"
msgstr ""

#: prefs.js:939
msgid "Alt"
msgstr ""

#: prefs.js:991
msgid "Press any key"
msgstr ""

#: prefs.js:1007
msgid "New Hotkey"
msgstr ""

#: prefs.js:1066
msgid "Menu Button"
msgstr ""

#: prefs.js:1071 prefs.js:1155
msgid "Text"
msgstr ""

#: prefs.js:1072
msgid "Icon and Text"
msgstr ""

#: prefs.js:1073
msgid "Text and Icon"
msgstr ""

#: prefs.js:1074
msgid "Hidden"
msgstr ""

#: prefs.js:1076
msgid "Appearance"
msgstr ""

#: prefs.js:1115
msgid "Padding"
msgstr ""

#: prefs.js:1167
msgid "Icon Settings"
msgstr ""

#: prefs.js:1170
msgid "Browse Icons"
msgstr ""

#: prefs.js:1206
msgid "Icon Size"
msgstr ""

#: prefs.js:1217
msgid "Menu Button Styling"
msgstr ""

#: prefs.js:1218 prefs.js:3098
msgid "Results may vary with third party themes"
msgstr ""

#: prefs.js:1222 prefs.js:1228 prefs.js:1234 prefs.js:3214 prefs.js:3240
#: prefs.js:3246
msgid "Foreground Color"
msgstr ""

#: prefs.js:1225 prefs.js:1228 prefs.js:3237 prefs.js:3240
msgid "Hover"
msgstr ""

#: prefs.js:1225 prefs.js:1231 prefs.js:3211 prefs.js:3237 prefs.js:3243
msgid "Background Color"
msgstr ""

#: prefs.js:1231 prefs.js:1234 prefs.js:3243 prefs.js:3246
msgid "Active"
msgstr ""

#: prefs.js:1237 prefs.js:3223
msgid "Border Radius"
msgstr ""

#: prefs.js:1240 prefs.js:3220
msgid "Border Width"
msgstr ""

#: prefs.js:1240
msgid "Background colors required if set to 0"
msgstr ""

#: prefs.js:1243 prefs.js:3217
msgid "Border Color"
msgstr ""

#: prefs.js:1387 prefs.js:1392
msgid "ArcMenu Icons"
msgstr ""

#: prefs.js:1415
msgid "Distro Icons"
msgstr ""

#: prefs.js:1451 prefs.js:1484
msgid "Custom Icon"
msgstr ""

#: prefs.js:1582
msgid "Legal disclaimer for Distro Icons"
msgstr ""

#: prefs.js:1638
msgid "Layouts"
msgstr ""

#: prefs.js:1681
msgid "Current Menu Layout"
msgstr ""

#: prefs.js:1696
msgid "Available Menu Layouts"
msgstr ""

#: prefs.js:1701 prefs.js:1869
#, javascript-format
msgid "%s Menu Layouts"
msgstr ""

#: prefs.js:1791
#, javascript-format
msgid "%s Layout Tweaks"
msgstr ""

#: prefs.js:1943
msgid "Menu Size"
msgstr ""

#: prefs.js:1999
msgid "Left-Panel Width"
msgstr ""

#: prefs.js:2000 prefs.js:2021 prefs.js:2162
msgid "Traditional Layouts"
msgstr ""

#: prefs.js:2020
msgid "Right-Panel Width"
msgstr ""

#: prefs.js:2041
msgid "Width Offset"
msgstr ""

#: prefs.js:2042
msgid "Non-Traditional Layouts"
msgstr ""

#: prefs.js:2054
msgid "Off"
msgstr ""

#: prefs.js:2055
msgid "Top Centered"
msgstr ""

#: prefs.js:2056
msgid "Bottom Centered"
msgstr ""

#: prefs.js:2058
msgid "Override Menu Location"
msgstr ""

#: prefs.js:2100
msgid "Override Menu Rise"
msgstr ""

#: prefs.js:2101
msgid "Menu Distance from Panel and Screen Edge"
msgstr ""

#: prefs.js:2122
msgid "Show Application Descriptions"
msgstr ""

#: prefs.js:2129
msgid "Full Color"
msgstr ""

#: prefs.js:2130
msgid "Symbolic"
msgstr ""

#: prefs.js:2132
msgid "Category Icon Type"
msgstr ""

#: prefs.js:2133 prefs.js:2144
msgid "Some icon themes may not include selected icon type"
msgstr ""

#: prefs.js:2143
msgid "Shortcuts Icon Type"
msgstr ""

#: prefs.js:2161
msgid "Vertical Separator"
msgstr ""

#: prefs.js:2169
msgid "Icon Sizes"
msgstr ""

#: prefs.js:2170
msgid "Modify the icon size of various menu elements."
msgstr ""

#: prefs.js:2175 prefs.js:2224
msgid "Default"
msgstr ""

#: prefs.js:2176 prefs.js:2179 prefs.js:2226
msgid "Small"
msgstr ""

#: prefs.js:2177 prefs.js:2180 prefs.js:2227
msgid "Medium"
msgstr ""

#: prefs.js:2178 prefs.js:2181 prefs.js:2228
msgid "Large"
msgstr ""

#: prefs.js:2179 prefs.js:2180 prefs.js:2181
msgid "Wide"
msgstr ""

#: prefs.js:2183
msgid "Grid Icons"
msgstr ""

#: prefs.js:2192
msgid "Categories &amp; Applications"
msgstr ""

#: prefs.js:2194
msgid "Buttons"
msgstr ""

#: prefs.js:2196
msgid "Quick Links"
msgstr ""

#: prefs.js:2198 prefs.js:2662
msgid "Misc"
msgstr ""

#: prefs.js:2225
msgid "Extra Small"
msgstr ""

#: prefs.js:2229
msgid "Extra Large"
msgstr ""

#: prefs.js:2271
msgid "Disable ScrollView Fade Effects"
msgstr ""

#: prefs.js:2287
msgid "Disable Tooltips"
msgstr ""

#: prefs.js:2303
msgid "Alphabetize 'All Programs' Category"
msgstr ""

#: prefs.js:2319
msgid "Show Hidden Recent Files"
msgstr ""

#: prefs.js:2339 prefs.js:2352
msgid "Multi-Lined Labels"
msgstr ""

#: prefs.js:2339
msgid "Enable/Disable multi-lined labels on large application icon layouts."
msgstr ""

#: prefs.js:2374
msgid "Disable New Apps Tracker"
msgstr ""

#: prefs.js:2384
msgid "Clear All"
msgstr ""

#: prefs.js:2393
msgid "Clear Apps Marked 'New'"
msgstr ""

#: prefs.js:2437
msgid "Extra Search Providers"
msgstr ""

#: prefs.js:2448
msgid "Search for open windows across all workspaces"
msgstr ""

#: prefs.js:2462
msgid "Search for recent files"
msgstr ""

#: prefs.js:2470 prefs.js:3531 prefs.js:3692
msgid "Search Options"
msgstr ""

#: prefs.js:2480
msgid "Show descriptions of search results"
msgstr ""

#: prefs.js:2495
msgid "Highlight search result terms"
msgstr ""

#: prefs.js:2514
msgid "Max Search Results"
msgstr ""

#: prefs.js:2669
msgid "Export or Import Settings"
msgstr ""

#: prefs.js:2679
msgid "Export or Import ArcMenu Settings"
msgstr ""

#: prefs.js:2680
msgid "Importing will overwrite current settings."
msgstr ""

#: prefs.js:2694
msgid "Import"
msgstr ""

#: prefs.js:2699
msgid "Import settings"
msgstr ""

#: prefs.js:2726
msgid "Export"
msgstr ""

#: prefs.js:2731
msgid "Export settings"
msgstr ""

#: prefs.js:2750
msgid "ArcMenu Settings Window Size"
msgstr ""

#: prefs.js:2766
msgid "Window Width"
msgstr ""

#: prefs.js:2786
msgid "Window Height"
msgstr ""

#: prefs.js:2795
msgid "Reset all ArcMenu Settings"
msgstr ""

#: prefs.js:2801
msgid "Reset all Settings"
msgstr ""

#: prefs.js:2807
msgid "Reset all settings?"
msgstr ""

#: prefs.js:2808
msgid "All ArcMenu settings will be reset to the default value."
msgstr ""

#: prefs.js:2884
msgid "Application Menu Extension for GNOME"
msgstr ""

#: prefs.js:2898
msgid "ArcMenu Version"
msgstr ""

#: prefs.js:2911
msgid "Git Commit"
msgstr ""

#: prefs.js:2924
msgid "GNOME Version"
msgstr ""

#: prefs.js:2932
msgid "OS"
msgstr ""

#: prefs.js:2955
msgid "Session Type"
msgstr ""

#: prefs.js:2972
msgid "Credits"
msgstr ""

#: prefs.js:3071
msgid "Theme"
msgstr ""

#: prefs.js:3097
msgid "Override Theme"
msgstr ""

#: prefs.js:3105
msgid "Menu Themes"
msgstr ""

#: prefs.js:3149
msgid "Current Theme"
msgstr ""

#: prefs.js:3177
msgid "Save as Theme"
msgstr ""

#: prefs.js:3206
msgid "Menu Styling"
msgstr ""

#: prefs.js:3229
msgid "Separator Color"
msgstr ""

#: prefs.js:3233
msgid "Menu Items Styling"
msgstr ""

#: prefs.js:3361
msgid "Save Theme As..."
msgstr ""

#: prefs.js:3372
msgid "Theme Name"
msgstr ""

#: prefs.js:3389
msgid "Save Theme"
msgstr ""

#: prefs.js:3408
msgid "Manage Themes"
msgstr ""

#: prefs.js:3497
msgid "Customize"
msgstr ""

#: prefs.js:3514 prefs.js:3525 prefs.js:3653
msgid "Menu Settings"
msgstr ""

#: prefs.js:3528 prefs.js:3680
msgid "Directory Shortcuts"
msgstr ""

#: prefs.js:3530 prefs.js:3688
msgid "Power Options"
msgstr ""

#: prefs.js:3532 prefs.js:3696
msgid "Extra Categories"
msgstr ""

#: prefs.js:3533 prefs.js:3700
msgid "Fine-Tune"
msgstr ""

#: prefs.js:3596
#, javascript-format
msgid "Reset all %s?"
msgstr ""

#: prefs.js:3597
#, javascript-format
msgid "All %s will be reset to the default value."
msgstr ""

#: prefs.js:3821
msgid "Invalid Shortcut"
msgstr ""

#: prefsWidgets.js:255
msgid "Modify"
msgstr ""

#: prefsWidgets.js:290
msgid "Move Up"
msgstr ""

#: prefsWidgets.js:306
msgid "Move Down"
msgstr ""

#: prefsWidgets.js:324
msgid "Delete"
msgstr ""

#: search.js:734
msgid "Searching..."
msgstr ""

#: search.js:736
msgid "No results."
msgstr ""

#: search.js:810
#, javascript-format
msgid "%d more"
msgid_plural "%d more"
msgstr[0] ""
msgstr[1] ""

#: searchProviders/openWindows.js:26
msgid "List of open windows across all workspaces"
msgstr ""

#: searchProviders/openWindows.js:27
msgid "Open Windows"
msgstr ""

#: searchProviders/openWindows.js:39
#, javascript-format
msgid "'%s' on Workspace %d"
msgstr ""

#: searchProviders/recentFiles.js:77
#, javascript-format
msgid "Failed to open “%s”"
msgstr ""

#: utils.js:27
msgid "ArcMenu - Hibernate Error!"
msgstr ""

#: utils.js:27
msgid "System unable to hibernate."
msgstr ""

#: utils.js:38
msgid "ArcMenu - Hybrid Sleep Error!"
msgstr ""

#: utils.js:38
msgid "System unable to hybrid sleep."
msgstr ""
